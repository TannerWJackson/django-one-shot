from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TaskForm


# Create your views here.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "todos/lists.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_list)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoForm(instance=todo_list)

    context = {
        "form": form,
    }
    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            todo_task = form.save()
            return redirect("todo_list_detail", id=todo_task.list.id)
    else:
        form = TaskForm()

    context = {
        "form": form,
    }
    return render(request, "todos/task_create.html", context)


def todo_item_update(request, id):
    todo_task = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=todo_task)
        if form.is_valid():
            todo_task = form.save()
            return redirect("todo_list_detail", id=todo_task.list.id)
    else:
        form = TaskForm(instance=todo_task)

    context = {
        "form": form,
    }
    return render(request, "todos/task_update.html", context)
